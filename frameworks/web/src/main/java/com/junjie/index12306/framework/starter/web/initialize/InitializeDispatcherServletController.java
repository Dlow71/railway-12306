

package com.junjie.index12306.framework.starter.web.initialize;

import com.junjie.index12306.framework.starter.web.config.WebAutoConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 初始化 {@link org.springframework.web.servlet.DispatcherServlet}
 * 定义伪 Controller 接口 让项目启动时自己调自己 提高springBoot第一次调用的接口反应速度
 *
 */
@Slf4j(topic = "Initialize DispatcherServlet")
@RestController
public final class InitializeDispatcherServletController {

    @GetMapping(WebAutoConfiguration.INITIALIZE_PATH)
    public void initializeDispatcherServlet() {
        log.info("Initialized the dispatcherServlet to improve the first response time of the interface...");
    }
}

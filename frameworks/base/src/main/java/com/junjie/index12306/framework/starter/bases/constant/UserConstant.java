

package com.junjie.index12306.framework.starter.bases.constant;

/**
 * 用户常量
 * 这种常量可能被多个模块用到，比如网关，用户等模块，所以抽象出来了
 * 
 */
public final class UserConstant {

    /**
     * 用户 ID Key
     */
    public static final String USER_ID_KEY = "userId";

    /**
     * 用户名 Key
     */
    public static final String USER_NAME_KEY = "username";

    /**
     * 用户真实名称 Key
     */
    public static final String REAL_NAME_KEY = "realName";

    /**
     * 用户 Token Key
     */
    public static final String USER_TOKEN_KEY = "token";
}

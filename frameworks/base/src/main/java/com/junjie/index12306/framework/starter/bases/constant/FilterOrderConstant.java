

package com.junjie.index12306.framework.starter.bases.constant;

/**
 * 全局过滤器顺序执行常量类
 * 思考下，过滤器、拦截器或 AOP 组件的执行顺序又为什么定义在基础组件库？
 * 正常来说，要先打印日志，再触发幂等行为。但是，如果说日志和幂等的组件库开发者不是同一人，幂等在日志请求之前。如果不触发幂等还好，一旦触发幂等行为抛出异常，那么日志组件将不再执行。这对于生产日志以及排查问题来说非常不友好。
 * 所以说，每个基础组件的执行顺序需要在全局定义，每个组件开发者要有全局思维，定义类似于这种过滤器或拦截器再或者 AOP 时，需要从组件功能再结合全局组件功能考虑到执行顺序问题。
 *
 */
public final class FilterOrderConstant {

    /**
     * 用户信息传递过滤器执行顺序排序
     */
    public static final int USER_TRANSMIT_FILTER_ORDER = 100;
}

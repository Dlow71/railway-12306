

package com.junjie.index12306.framework.starter.idempotent.core.token;

import com.junjie.index12306.framework.starter.convention.result.Result;
import lombok.RequiredArgsConstructor;

import com.junjie.index12306.framework.starter.web.Results;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 基于 Token 验证请求幂等性控制器
 *
 *
 */
@RestController
@RequiredArgsConstructor
public class IdempotentTokenController {

    private final IdempotentTokenService idempotentTokenService;

    /**
     * 请求申请Token
     * 获取了token后，才能拿着这个token去访问某个接口，保证幂等性
     */
    @GetMapping("/token")
    public Result<String> createToken() {
        return Results.success(idempotentTokenService.createToken());
    }
}



package com.junjie.index12306.biz.orderservice.mq.consumer;

import com.junjie.index12306.biz.orderservice.common.constant.OrderRocketMQConstant;
import com.junjie.index12306.biz.orderservice.common.enums.OrderItemStatusEnum;
import com.junjie.index12306.biz.orderservice.common.enums.OrderStatusEnum;
import com.junjie.index12306.biz.orderservice.dto.domain.OrderStatusReversalDTO;
import com.junjie.index12306.biz.orderservice.mq.event.PayResultCallbackOrderEvent;
import com.junjie.index12306.biz.orderservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import com.junjie.index12306.biz.orderservice.mq.domain.MessageWrapper;
import com.junjie.index12306.framework.starter.idempotent.annotation.Idempotent;
import com.junjie.index12306.framework.starter.idempotent.enums.IdempotentSceneEnum;
import com.junjie.index12306.framework.starter.idempotent.enums.IdempotentTypeEnum;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 支付结果回调订单消费者
 *
 *
 */
@Slf4j
@Component
@RequiredArgsConstructor
@RocketMQMessageListener(
        topic = OrderRocketMQConstant.PAY_GLOBAL_TOPIC_KEY,
        selectorExpression = OrderRocketMQConstant.PAY_RESULT_CALLBACK_TAG_KEY,
        consumerGroup = OrderRocketMQConstant.PAY_RESULT_CALLBACK_ORDER_CG_KEY
)
public class PayResultCallbackOrderConsumer implements RocketMQListener<MessageWrapper<PayResultCallbackOrderEvent>> {

    private final OrderService orderService;

    @Idempotent(
            uniqueKeyPrefix = "index12306-order:pay_result_callback:",
            key = "#message.getKeys()+'_'+#message.hashCode()",
            type = IdempotentTypeEnum.SPEL,
            scene = IdempotentSceneEnum.MQ,
            keyTimeout = 7200L
    )
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void onMessage(MessageWrapper<PayResultCallbackOrderEvent> message) {
        PayResultCallbackOrderEvent payResultCallbackOrderEvent = message.getMessage();
        OrderStatusReversalDTO orderStatusReversalDTO = OrderStatusReversalDTO.builder()
                .orderSn(payResultCallbackOrderEvent.getOrderSn())
                .orderStatus(OrderStatusEnum.ALREADY_PAID.getStatus())
                .orderItemStatus(OrderItemStatusEnum.ALREADY_PAID.getStatus())
                .build();
        // 订单状态反转 就单纯修改订单状态
        orderService.statusReversal(orderStatusReversalDTO);
        // 支付结果回调订单 就是修改主订单中的 用户支付时间以及支付渠道
        orderService.payCallbackOrder(payResultCallbackOrderEvent);
    }
}



package com.junjie.index12306.biz.ticketservice.job;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.junjie.index12306.biz.ticketservice.common.constant.Index12306Constant;
import com.junjie.index12306.biz.ticketservice.common.constant.RedisKeyConstant;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.RequiredArgsConstructor;
import com.junjie.index12306.biz.ticketservice.dao.entity.RegionDO;
import com.junjie.index12306.biz.ticketservice.dao.entity.TrainStationRelationDO;
import com.junjie.index12306.biz.ticketservice.dao.mapper.RegionMapper;
import com.junjie.index12306.biz.ticketservice.dao.mapper.TrainStationRelationMapper;
import com.junjie.index12306.framework.starter.cache.DistributedCache;
import com.junjie.index12306.framework.starter.common.toolkit.EnvironmentUtil;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 地区站点查询定时任务
 *
 *
 */
@RestController
@RequiredArgsConstructor
public class RegionTrainStationJobHandler extends IJobHandler {

    private final RegionMapper regionMapper;
    private final TrainStationRelationMapper trainStationRelationMapper;
    private final DistributedCache distributedCache;

    /**
     * 整个过程就是遍历地区组合，收集各地区间的火车线路关系数据，
     * 并将这些数据持久化到Redis中，以便后续快速查询特定日期的火车线路情况。
     */
    @XxlJob(value = "regionTrainStationJobHandler")
    @GetMapping("/api/ticket-service/region-train-station/job/cache-init/execute")
    @Override
    public void execute() {
        // 1、获取所有地区的名称
        List<String> regionList = regionMapper.selectList(Wrappers.emptyWrapper())
                .stream()
                .map(RegionDO::getName)
                .collect(Collectors.toList());
        String requestParam = getJobRequestParam();
        var dateTime = StrUtil.isNotBlank(requestParam) ? requestParam : DateUtil.tomorrow().toDateStr();
        // 2、双重循环遍历地区名称 找到所有不同地域的匹配
        for (int i = 0; i < regionList.size(); i++) {
            for (int j = 0; j < regionList.size(); j++) {
                if (i != j) {
                    String startRegion = regionList.get(i);
                    String endRegion = regionList.get(j);
                    // 3、查询当前出发地到目的地的 列车站点关系实体
                    LambdaQueryWrapper<TrainStationRelationDO> relationQueryWrapper = Wrappers.lambdaQuery(TrainStationRelationDO.class)
                            .eq(TrainStationRelationDO::getStartRegion, startRegion)
                            .eq(TrainStationRelationDO::getEndRegion, endRegion);
                    List<TrainStationRelationDO> trainStationRelationDOList = trainStationRelationMapper.selectList(relationQueryWrapper);
                    if (CollUtil.isEmpty(trainStationRelationDOList)) {
                        continue;
                    }
                    // 4、遍历关系实体集合 并按照出发时间进行一个排序并塞入到ZSet(redis)有序集合中
                    // 创建一个HashSet<ZSetOperations.TypedTuple<String>>类型的变量tuples，用于存储要添加到Redis有序集合中的元素
                    Set<ZSetOperations.TypedTuple<String>> tuples = new HashSet<>();
                    for (TrainStationRelationDO item : trainStationRelationDOList) {
                        String zSetKey = StrUtil.join("_", item.getTrainId(), item.getDeparture(), item.getArrival());
                        ZSetOperations.TypedTuple<String> tuple = ZSetOperations.TypedTuple.of(zSetKey, Double.valueOf(item.getDepartureTime().getTime()));
                        tuples.add(tuple);
                    }
                    // 5、存入redis并设置过期时间
                    StringRedisTemplate stringRedisTemplate = (StringRedisTemplate) distributedCache.getInstance();
                    String buildCacheKey = RedisKeyConstant.REGION_TRAIN_STATION + StrUtil.join("_", startRegion, endRegion, dateTime);
                    stringRedisTemplate.opsForZSet().add(buildCacheKey, tuples);
                    stringRedisTemplate.expire(buildCacheKey, Index12306Constant.ADVANCE_TICKET_DAY, TimeUnit.DAYS);
                }
            }
        }
    }

    private String getJobRequestParam() {
        return EnvironmentUtil.isDevEnvironment()
                ? ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getHeader("requestParam")
                : XxlJobHelper.getJobParam();
    }
}

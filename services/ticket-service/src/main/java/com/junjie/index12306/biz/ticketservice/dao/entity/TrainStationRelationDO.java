

package com.junjie.index12306.biz.ticketservice.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.junjie.index12306.framework.starter.database.base.BaseDO;

import java.util.Date;

/**
 * 列车站点关系实体
 * 比如 长沙 -> 广州 -> 杭州 -> 上海  总共会存储6条记录，存的每个站点之间关系
 * 主要是用来扣减库存用的 比如买了长沙到上海的票，就要把经过的站点所有库存减掉
 * 
 */
@Data
@TableName("t_train_station_relation")
public class TrainStationRelationDO extends BaseDO {

    /**
     * id
     */
    private Long id;

    /**
     * 车次id
     */
    private Long trainId;

    /**
     * 出发站点
     */
    private String departure;

    /**
     * 到达站点
     */
    private String arrival;

    /**
     * 起始城市
     */
    private String startRegion;

    /**
     * 终点城市
     */
    private String endRegion;

    /**
     * 始发站标识
     */
    private Boolean departureFlag;

    /**
     * 终点站标识
     */
    private Boolean arrivalFlag;

    /**
     * 出发时间
     */
    private Date departureTime;

    /**
     * 到达时间
     */
    private Date arrivalTime;
}



package com.junjie.index12306.biz.ticketservice.dto.resp;

import com.junjie.index12306.biz.ticketservice.dto.domain.TicketListDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 车票分页查询响应参数
 *
 * 
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TicketPageQueryRespDTO {

    /**
     * 车次集合数据
     */
    private List<TicketListDTO> trainList;

    /**
     * 车次类型：D-动车 Z-直达 复兴号等
     */
    private List<Integer> trainBrandList;

    /**
     * 出发车站 比如北京就有两个车站 北京南 和 北京
     */
    private List<String> departureStationList;

    /**
     * 到达车站 与出发车站同理
     */
    private List<String> arrivalStationList;

    /**
     * 车次席别 比如该车次有 一等座 二等座 ，不过都是用数字表示的
     */
    private List<Integer> seatClassTypeList;
}



package com.junjie.index12306.biz.ticketservice.controller;

import com.junjie.index12306.biz.ticketservice.dto.req.CancelTicketOrderReqDTO;
import com.junjie.index12306.biz.ticketservice.dto.req.PurchaseTicketReqDTO;
import com.junjie.index12306.biz.ticketservice.dto.req.RefundTicketReqDTO;
import com.junjie.index12306.biz.ticketservice.dto.req.TicketPageQueryReqDTO;
import com.junjie.index12306.biz.ticketservice.dto.resp.RefundTicketRespDTO;
import com.junjie.index12306.biz.ticketservice.dto.resp.TicketPageQueryRespDTO;
import com.junjie.index12306.biz.ticketservice.dto.resp.TicketPurchaseRespDTO;
import com.junjie.index12306.biz.ticketservice.remote.dto.PayInfoRespDTO;
import lombok.RequiredArgsConstructor;
import com.junjie.index12306.biz.ticketservice.service.TicketService;
import com.junjie.index12306.framework.starter.convention.result.Result;
import com.junjie.index12306.framework.starter.idempotent.annotation.Idempotent;
import com.junjie.index12306.framework.starter.idempotent.enums.IdempotentSceneEnum;
import com.junjie.index12306.framework.starter.idempotent.enums.IdempotentTypeEnum;
import com.junjie.index12306.framework.starter.log.annotation.ILog;
import com.junjie.index12306.framework.starter.web.Results;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 车票控制层
 *
 *
 */
@RestController
@RequiredArgsConstructor
public class TicketController {

    private final TicketService ticketService;

    /**
     * 根据条件查询车票
     * 一般只有修改了出发地和目的地和刚进页面，才会调用该接口
     */
    @GetMapping("/api/ticket-service/ticket/query")
    public Result<TicketPageQueryRespDTO> pageListTicketQuery(TicketPageQueryReqDTO requestParam) {
        return Results.success(ticketService.pageListTicketQueryV1(requestParam));
    }

    /**
     * 购买车票
     */
    @ILog
    @Idempotent(
            uniqueKeyPrefix = "index12306-ticket:lock_purchase-tickets:",
            key = "T(com.junjie.index12306.framework.starter.bases.ApplicationContextHolder).getBean('environment').getProperty('unique-name', '')"
                    + "+'_'+"
                    + "T(com.junjie.index12306.frameworks.starter.user.core.UserContext).getUsername()",
            message = "正在执行下单流程，请稍后...",
            scene = IdempotentSceneEnum.RESTAPI,
            type = IdempotentTypeEnum.SPEL
    )
    @PostMapping("/api/ticket-service/ticket/purchase")
    public Result<TicketPurchaseRespDTO> purchaseTickets(@RequestBody PurchaseTicketReqDTO requestParam) {
        return Results.success(ticketService.purchaseTicketsV1(requestParam));
    }

    /**
     * 购买车票v2
     */
    @ILog
    @Idempotent(
            uniqueKeyPrefix = "index12306-ticket:lock_purchase-tickets:",
            key = "T(com.junjie.index12306.framework.starter.bases.ApplicationContextHolder).getBean('environment').getProperty('unique-name', '')"
                    + "+'_'+"
                    + "T(com.junjie.index12306.frameworks.starter.user.core.UserContext).getUsername()",
            message = "正在执行下单流程，请稍后...",
            scene = IdempotentSceneEnum.RESTAPI,
            type = IdempotentTypeEnum.SPEL
    )
    @PostMapping("/api/ticket-service/ticket/purchase/v2")
    public Result<TicketPurchaseRespDTO> purchaseTicketsV2(@RequestBody PurchaseTicketReqDTO requestParam) {
        return Results.success(ticketService.purchaseTicketsV2(requestParam));
    }

    /**
     * 手动取消车票订单
     */
    @ILog
    @PostMapping("/api/ticket-service/ticket/cancel")
    public Result<Void> cancelTicketOrder(@RequestBody CancelTicketOrderReqDTO requestParam) {
        ticketService.cancelTicketOrder(requestParam);
        return Results.success();
    }

    /**
     * 支付单详情查询
     */
    @GetMapping("/api/ticket-service/ticket/pay/query")
    public Result<PayInfoRespDTO> getPayInfo(@RequestParam(value = "orderSn") String orderSn) {
        return Results.success(ticketService.getPayInfo(orderSn));
    }

    /**
     * 公共退款接口
     */
    @PostMapping("/api/ticket-service/ticket/refund")
    public Result<RefundTicketRespDTO> commonTicketRefund(@RequestBody RefundTicketReqDTO requestParam) {
        return Results.success(ticketService.commonTicketRefund(requestParam));
    }
}

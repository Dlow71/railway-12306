

package com.junjie.index12306.biz.ticketservice.service.handler.ticket.tokenbucket;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.collect.Lists;
import com.junjie.index12306.biz.ticketservice.common.constant.Index12306Constant;
import com.junjie.index12306.biz.ticketservice.common.constant.RedisKeyConstant;
import com.junjie.index12306.biz.ticketservice.common.enums.VehicleTypeEnum;
import com.junjie.index12306.biz.ticketservice.dao.entity.TrainDO;
import com.junjie.index12306.biz.ticketservice.dao.mapper.SeatMapper;
import com.junjie.index12306.biz.ticketservice.dao.mapper.TrainMapper;
import com.junjie.index12306.biz.ticketservice.dto.domain.PurchaseTicketPassengerDetailDTO;
import com.junjie.index12306.biz.ticketservice.dto.domain.RouteDTO;
import com.junjie.index12306.biz.ticketservice.dto.domain.SeatTypeCountDTO;
import com.junjie.index12306.biz.ticketservice.dto.req.PurchaseTicketReqDTO;
import com.junjie.index12306.biz.ticketservice.remote.dto.TicketOrderDetailRespDTO;
import com.junjie.index12306.biz.ticketservice.remote.dto.TicketOrderPassengerDetailRespDTO;
import com.junjie.index12306.biz.ticketservice.service.TrainStationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.junjie.index12306.framework.starter.bases.Singleton;
import com.junjie.index12306.framework.starter.cache.DistributedCache;
import com.junjie.index12306.framework.starter.common.toolkit.Assert;
import com.junjie.index12306.framework.starter.convention.exception.ServiceException;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 列车车票余量令牌桶，应对海量并发场景下满足并行、限流以及防超卖等场景
 * 主要做了三件事：
 * 1. 如果令牌容器在缓存中失效需要重新读取并放入缓存；
 * 2. 准备执行 Lua 脚本的数据；
 * 3. 最终的执行 Lua 脚本获取令牌。
 *
 * 令牌限流容器中的数据，是与列车余票一一对应的。
 */
@Slf4j
@Component
@RequiredArgsConstructor
public final class TicketAvailabilityTokenBucket {

    private final TrainStationService trainStationService;
    private final DistributedCache distributedCache;
    private final RedissonClient redissonClient;
    private final SeatMapper seatMapper;
    private final TrainMapper trainMapper;

    private static final String LUA_TICKET_AVAILABILITY_TOKEN_BUCKET_PATH = "lua/ticket_availability_token_bucket.lua";
    private static final String LUA_TICKET_AVAILABILITY_ROLLBACK_TOKEN_BUCKET_PATH = "lua/ticket_availability_rollback_token_bucket.lua";

    /**
     * 获取车站间令牌桶中的令牌访问
     * 如果返回 {@link Boolean#TRUE} 代表可以参与接下来的购票下单流程
     * 如果返回 {@link Boolean#FALSE} 代表当前访问出发站点和到达站点令牌已被拿完，无法参与购票下单等逻辑
     * 为了避免 Redis 极端场景下的数据被删除或者失效等问题，我们这里考虑到这种情况所以先通过判断令牌容器是否存在，存在则继续，不存在则进行加载。
     * @param requestParam 购票请求参数入参
     * @return 是否获取列车车票余量令牌桶中的令牌，{@link Boolean#TRUE} or {@link Boolean#FALSE}
     */
    public boolean takeTokenFromBucket(PurchaseTicketReqDTO requestParam) {
        // 1、获取列车信息
        TrainDO trainDO = distributedCache.safeGet(
                RedisKeyConstant.TRAIN_INFO + requestParam.getTrainId(),
                TrainDO.class,
                () -> trainMapper.selectById(requestParam.getTrainId()),
                Index12306Constant.ADVANCE_TICKET_DAY,
                TimeUnit.DAYS);
        // 2、获取列车经停站之间的数据集合(路线)，因为一旦失效要读取整个列车的令牌并重新赋值
        List<RouteDTO> routeDTOList = trainStationService
                .listTrainStationRoute(requestParam.getTrainId(), trainDO.getStartStation(), trainDO.getEndStation());
        StringRedisTemplate stringRedisTemplate = (StringRedisTemplate) distributedCache.getInstance();
        // 3、令牌容器是个 Hash 结构，组装令牌 HashKey
        String actualHashKey = RedisKeyConstant.TICKET_AVAILABILITY_TOKEN_BUCKET + requestParam.getTrainId();
        // 4、判断令牌容器是否存在 为了避免 Redis 极端场景下的数据被删除或者失效等问题，我们这里考虑到这种情况所以先通过判断令牌容器是否存在，存在则继续，不存在则进行加载。
        Boolean hasKey = distributedCache.hasKey(actualHashKey);
        // 5、令牌容器 Hash 数据结构不存在了，执行加载流程
        if (!hasKey) {
            // 5.1、双重判定锁，避免加载数据请求多次请求数据库
            RLock lock = redissonClient.getLock(String.format(RedisKeyConstant.LOCK_TICKET_AVAILABILITY_TOKEN_BUCKET, requestParam.getTrainId()));
            lock.lock();
            try {
                Boolean hasKeyTwo = distributedCache.hasKey(actualHashKey);
                if (!hasKeyTwo) {
                    // 5.2、根据当前列车的列车类型 获取出所有的座位类型
                    List<Integer> seatTypes = VehicleTypeEnum.findSeatTypesByCode(trainDO.getTrainType());
                    Map<String, String> ticketAvailabilityTokenMap = new HashMap<>();
                    for (RouteDTO each : routeDTOList) {
                        // 5.3、获取列车 startStation 到 endStation 区间可用的所有座位集合
                        List<SeatTypeCountDTO> seatTypeCountDTOList = seatMapper.listSeatTypeCount(Long.parseLong(requestParam.getTrainId()), each.getStartStation(), each.getEndStation(), seatTypes);
                        // 5.4、挨个放入map中
                        for (SeatTypeCountDTO eachSeatTypeCountDTO : seatTypeCountDTOList) {
                            // 组装HashKey
                            String buildCacheKey = StrUtil.join("_", each.getStartStation(), each.getEndStation(), eachSeatTypeCountDTO.getSeatType());
                            // 一个 Hash 结构下有很多 Key，为了避免多次网络 IO，这里组装成一个本地 Map，通过 putAll 方法请求一次 Redis
                            ticketAvailabilityTokenMap.put(buildCacheKey, String.valueOf(eachSeatTypeCountDTO.getSeatCount()));
                        }
                    }
                    // 5.5、一次全部set到redis中
                    stringRedisTemplate.opsForHash().putAll(RedisKeyConstant.TICKET_AVAILABILITY_TOKEN_BUCKET + requestParam.getTrainId(), ticketAvailabilityTokenMap);
                }
            } finally {
                lock.unlock();
            }
        }
        // 6、获取到 Redis 执行的 Lua 脚本 这里很细节的就是使用单例对象容器 不用每次都重新获取
        DefaultRedisScript<Long> actual = Singleton.get(LUA_TICKET_AVAILABILITY_TOKEN_BUCKET_PATH, () -> {
            DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>();
            redisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource(LUA_TICKET_AVAILABILITY_TOKEN_BUCKET_PATH)));
            redisScript.setResultType(Long.class);
            return redisScript;
        });
        // 7、判断 Lua 脚本不是空的，这一步基本上可以说是永远不会出错，为了避免 IDEA 波浪线才加的
        Assert.notNull(actual);
        // 8、因为购票时，一个用户可以为多个乘车人买票，而多个乘车人又能购买不同的票，所以这里需要根据座位类型进行分组
        // Collectors.counting()则是对每一个分组内部的元素进行计数，得到每一种座位类型的乘客数量。
        Map<Integer, Long> seatTypeCountMap = requestParam.getPassengers().stream()
                .collect(Collectors.groupingBy(PurchaseTicketPassengerDetailDTO::getSeatType, Collectors.counting()));
        // 9、最终结构就是拆分为一个 Map，Key 是座位类型，value 是该座位类型的购票人数
        JSONArray seatTypeCountArray = seatTypeCountMap.entrySet().stream()
                .map(entry -> {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("seatType", String.valueOf(entry.getKey()));
                    jsonObject.put("count", String.valueOf(entry.getValue()));
                    return jsonObject;
                })
                .collect(Collectors.toCollection(JSONArray::new));
        // 10、获取需要判断扣减的站点
        List<RouteDTO> takeoutRouteDTOList = trainStationService
                .listTakeoutTrainStationRoute(requestParam.getTrainId(), requestParam.getDeparture(), requestParam.getArrival());
        // 用户购买的出发站点和到达站点
        String luaScriptKey = StrUtil.join("_", requestParam.getDeparture(), requestParam.getArrival());
        // 11、执行lua脚本开始扣减 luaScriptKey：用户购买的出发站点和到达站点，比如北京南_南京南
        // seatTypeCountArray：需要扣减的座位类型以及对应数量 takeoutRouteDTOList：需要扣减的相关列车站点
        // 因为要操作多个 Hash Key，这个多站点的自减是非原子性的，所以我们需要通过 LUA 脚本完成。
        Long result = stringRedisTemplate.execute(actual, Lists.newArrayList(actualHashKey, luaScriptKey), JSON.toJSONString(seatTypeCountArray), JSON.toJSONString(takeoutRouteDTOList));
        // 根据返回值判断获取令牌是否成功
        return result != null && Objects.equals(result, 0L);
    }

    /**
     * 回滚列车余量令牌，一般为订单取消或长时间未支付触发
     *
     * @param requestParam 回滚列车余量令牌入参
     */
    public void rollbackInBucket(TicketOrderDetailRespDTO requestParam) {
        // 1、获取回滚lua脚本
        DefaultRedisScript<Long> actual = Singleton.get(LUA_TICKET_AVAILABILITY_ROLLBACK_TOKEN_BUCKET_PATH, () -> {
            DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>();
            redisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource(LUA_TICKET_AVAILABILITY_ROLLBACK_TOKEN_BUCKET_PATH)));
            redisScript.setResultType(Long.class);
            return redisScript;
        });
        Assert.notNull(actual);
        // 2、获取子订单列表并根据座位分组并统计count
        List<TicketOrderPassengerDetailRespDTO> passengerDetails = requestParam.getPassengerDetails();
        Map<Integer, Long> seatTypeCountMap = passengerDetails.stream()
                .collect(Collectors.groupingBy(TicketOrderPassengerDetailRespDTO::getSeatType, Collectors.counting()));
        JSONArray seatTypeCountArray = seatTypeCountMap.entrySet().stream()
                .map(entry -> {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("seatType", String.valueOf(entry.getKey()));
                    jsonObject.put("count", String.valueOf(entry.getValue()));
                    return jsonObject;
                })
                .collect(Collectors.toCollection(JSONArray::new));
        // 3、拼接RedisKey
        StringRedisTemplate stringRedisTemplate = (StringRedisTemplate) distributedCache.getInstance();
        String actualHashKey = RedisKeyConstant.TICKET_AVAILABILITY_TOKEN_BUCKET + requestParam.getTrainId();
        String luaScriptKey = StrUtil.join("_", requestParam.getDeparture(), requestParam.getArrival());
        // 4、查出路线 然后执行回滚lua脚本
        List<RouteDTO> takeoutRouteDTOList = trainStationService.listTakeoutTrainStationRoute(String.valueOf(requestParam.getTrainId()), requestParam.getDeparture(), requestParam.getArrival());
        Long result = stringRedisTemplate.execute(actual, Lists.newArrayList(actualHashKey, luaScriptKey), JSON.toJSONString(seatTypeCountArray), JSON.toJSONString(takeoutRouteDTOList));
        if (result == null || !Objects.equals(result, 0L)) {
            log.error("回滚列车余票令牌失败，订单信息：{}", JSON.toJSONString(requestParam));
            throw new ServiceException("回滚列车余票令牌失败");
        }
    }

    public void putTokenInBucket() {

    }

    public void initializeTokens() {

    }
}



package com.junjie.index12306.biz.ticketservice.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.junjie.index12306.biz.ticketservice.dao.entity.TrainStationRelationDO;

/**
 * 列车站点关系持久层
 *
 *
 */
public interface TrainStationRelationMapper extends BaseMapper<TrainStationRelationDO> {
}

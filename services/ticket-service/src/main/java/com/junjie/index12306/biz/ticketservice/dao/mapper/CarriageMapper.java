

package com.junjie.index12306.biz.ticketservice.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.junjie.index12306.biz.ticketservice.dao.entity.CarriageDO;

/**
 * 车厢实体
 *
 *
 */
public interface CarriageMapper extends BaseMapper<CarriageDO> {
}

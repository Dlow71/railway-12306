

package com.junjie.index12306.biz.aggregationservice;

import cn.crane4j.spring.boot.annotation.EnableCrane4j;
import cn.hippo4j.core.enable.EnableDynamicThreadPool;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.retry.annotation.EnableRetry;

/**
 * 12306 聚合服务应用启动器
 *
 *
 */
@EnableDynamicThreadPool
@SpringBootApplication(scanBasePackages = {
        "com.junjie.index12306.biz.userservice",
        "com.junjie.index12306.biz.ticketservice",
        "com.junjie.index12306.biz.orderservice",
        "com.junjie.index12306.biz.payservice"
})
@EnableRetry
@MapperScan(value = {
        "com.junjie.index12306.biz.userservice.dao.mapper",
        "com.junjie.index12306.biz.ticketservice.dao.mapper",
        "com.junjie.index12306.biz.orderservice.dao.mapper",
        "com.junjie.index12306.biz.payservice.dao.mapper"
})
@EnableFeignClients(value = {
        "com.junjie.index12306.biz.ticketservice.remote",
        "com.junjie.index12306.biz.orderservice.remote"
})
@EnableCrane4j(enumPackages = "com.junjie.index12306.biz.orderservice.common.enums")
public class AggregationServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AggregationServiceApplication.class, args);
    }
}



package com.junjie.index12306.biz.gatewayservice.config;

import lombok.Data;

import java.util.List;

/**
 * 过滤器配置
 * 自定义配置类
 * 需要在路由配置中引用它，并传入相应的配置参数
 * 配置写在args下
 */
@Data
public class Config {

    /**
     * 黑名单前置路径
     */
    private List<String> blackPathPre;
}

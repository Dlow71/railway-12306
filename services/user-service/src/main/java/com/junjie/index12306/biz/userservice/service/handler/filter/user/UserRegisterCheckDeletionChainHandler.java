

package com.junjie.index12306.biz.userservice.service.handler.filter.user;

import lombok.RequiredArgsConstructor;
import com.junjie.index12306.biz.userservice.dto.req.UserRegisterReqDTO;
import com.junjie.index12306.biz.userservice.service.UserService;
import com.junjie.index12306.framework.starter.convention.exception.ClientException;
import org.springframework.stereotype.Component;

/**
 * 用户注册检查证件号是否多次注销
 * 如果用户频繁申请账号再注销，会导致RedisSet过大，增大存储和查询负担
 * 所以这里每次用户注销时，记录用户的证件号，并限制证件号仅可用于注销五次。超过这个限制的次数，将禁止该证件号再次用于注册账号。
 */
@Component
@RequiredArgsConstructor
public final class UserRegisterCheckDeletionChainHandler implements UserRegisterCreateChainFilter<UserRegisterReqDTO> {

    private final UserService userService;

    @Override
    public void handler(UserRegisterReqDTO requestParam) {
        Integer userDeletionNum = userService.queryUserDeletionNum(requestParam.getIdType(), requestParam.getIdCard());
        if (userDeletionNum >= 5) {
            throw new ClientException("证件号多次注销账号已被加入黑名单");
        }
    }

    @Override
    public int getOrder() {
        return 2;
    }
}

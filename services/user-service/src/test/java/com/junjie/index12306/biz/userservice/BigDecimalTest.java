package com.junjie.index12306.biz.userservice;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class BigDecimalTest {
    @Test
    public void test(){
        BigDecimal a = new BigDecimal("1");
        BigDecimal b = new BigDecimal("1.0");
        System.out.println(a.compareTo(b));
        System.out.println(a.equals(b));
    }
}

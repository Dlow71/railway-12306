

package com.junjie.index12306.biz.wxservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 微信服务应用启动器
 *
 *
 */
@SpringBootApplication
@EnableAsync
@MapperScan("com.junjie.index12306.biz.wxservice.dao.mapper")
public class WxServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(WxServiceApplication.class, args);
    }
}

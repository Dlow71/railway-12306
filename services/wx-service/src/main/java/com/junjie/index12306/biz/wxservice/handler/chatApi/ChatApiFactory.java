package com.junjie.index12306.biz.wxservice.handler.chatApi;

import com.junjie.index12306.biz.wxservice.handler.message.WxChatMsgHandler;
import com.junjie.index12306.biz.wxservice.handler.message.WxChatMsgTypeEnum;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 策略模式工厂
 */
@Component
public class ChatApiFactory implements InitializingBean {

    @Autowired
    private List<ChatApiHandler> chatApiHandlerList;

    private Map<ChatApiTypeEnum, ChatApiHandler> handlerMap = new HashMap<>();

    public ChatApiHandler getHandlerByChatApiType(String chatApiType) {
        ChatApiTypeEnum chatApiTypeEnum = ChatApiTypeEnum.getApiType(chatApiType);
        return handlerMap.get(chatApiTypeEnum);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        for (ChatApiHandler chatApiHandler : chatApiHandlerList) {
            handlerMap.put(chatApiHandler.getChatApiType(), chatApiHandler);
        }
    }

}

package com.junjie.index12306.biz.wxservice.handler.chatApi;

import com.junjie.index12306.biz.wxservice.handler.message.WxChatMsgTypeEnum;

import java.util.Map;

public interface ChatApiHandler {

    ChatApiTypeEnum getChatApiType();

    String dealMsgAndCall(String message);

}

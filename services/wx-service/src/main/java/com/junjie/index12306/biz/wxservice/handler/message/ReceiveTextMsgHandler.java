package com.junjie.index12306.biz.wxservice.handler.message;


import com.junjie.index12306.biz.wxservice.handler.chatApi.ChatApiTemplate;
import com.junjie.index12306.biz.wxservice.utils.RedisUtil;
import com.junjie.index12306.biz.wxservice.utils.MessageUtil;
import com.junjie.index12306.framework.starter.cache.DistributedCache;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * 公众号收到消息后的消息返回处理
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class ReceiveTextMsgHandler implements WxChatMsgHandler {
    private static final String KEY_WORD = "验证码";
    private static final String LINK = "link";
    private static final String TRAIN = "12306";
    private static final String LOGIN_PREFIX = "loginCode";

    private final RedisUtil redisUtil;
    private final ChatApiTemplate chatApiTemplate;


    @Override
    public WxChatMsgTypeEnum getMsgType() {
        return WxChatMsgTypeEnum.TEXT_MSG;
    }

    @Override
    public String dealMsg(Map<String, String> messageMap) {
        log.info("接收到文本消息事件");
        String content = messageMap.get("Content");
        String fromUserName = messageMap.get("FromUserName");
        String toUserName = messageMap.get("ToUserName");
        String MsgId = messageMap.get("MsgId");
        String aliPaySand = "支付宝沙箱测试号：\n" +
                "账号：ahlbks5547@sandbox.com\n" +
                "密码：111111\n" +
                "支付密码：111111";

        if (KEY_WORD.equals(content) && false) {  // 公众号登录，暂未开放
            // 返回一个随机有效码给用户 并存储到Redis中
            Random random = new Random();
            int num = random.nextInt(1000);
            String numContent = "您当前的验证码是：" + num + "！ 5分钟内有效";
            String numKey = redisUtil.buildKey(LOGIN_PREFIX, String.valueOf(num));
            // 这里的fromUserName就是openId
            redisUtil.setNx(numKey, fromUserName, 5L, TimeUnit.MINUTES);
            // fromUser和toUser反过来就可以给用户发送消息了
            return numContent;
        }else if(LINK.equals(content)){
            String reply = "SaaS短链接演示环境验证码：admin123456";
            return reply;
        }else if(TRAIN.equals(content)){
            String reply = "12306 人机识别验证码：admin123456";
            return reply;
        }else if(matchAiChat(content,"ai1") || matchAiChat(content,"ai2")){
            String reply = chatApiTemplate.callChatAi(content,fromUserName,MsgId);
            return reply;
        }else if(matchAiChat(content,"获取结果")){
            String reply = chatApiTemplate.tryGetResult(content);
            return reply;
        }else if(content.equals("ai")){
//            return "有两种AI回复方式：\n" +
//                    "1、使用ai1/问题，是进行短内容回复，15秒内回复完毕，如果没有回复，请再问一次" +
//                    "\n因为有的问题可能生成超过15秒，但是微信回复的回调接口只有十五秒\n" +
//                    "使用示例：ai1/西红柿炒蛋的制作方式\n" +
//                    "2、使用ai2/问题，是进行长内容回复，调用之后返回获取结果唯一码\n" +
//                    "使用获取结果/唯一码，即可获取结果\n" +
//                    "使用示例：ai2/西红柿炒蛋的制作方式\n" +
//                    "获取结果/3KrftQ";
            // ai1废弃了暂时
            return "AI回复方式：\n" +
                    "使用ai2/问题，调用之后返回获取结果唯一码\n" +
                    "使用获取结果/唯一码，即可获取结果\n" +
                    "使用示例：ai2/西红柿炒蛋的制作方式\n" +
                    "获取结果/3KrftQ";
        }else if(content.equals("支付宝沙箱测试号")){
            return aliPaySand;
        }
        return "";
    }

    private boolean matchAiChat(String content,String prefix) {
        String[] split = content.split("/",2);
        if(split.length==2 && split[0].equals(prefix)){
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        String content = "ai/132/123";
        System.out.println(content);
        String[] split = content.split("/",2);
        content = content.replace("ai/","");
//        System.out.println(split.toString());
//        System.out.println(content);

        String xml = "AI无法“瘦下来”，但若人成功瘦身，会发生多方面变化：\n" +
                "\n" +
                "1. 外形改变：身体线条更显紧致，脸部轮廓清晰，穿衣风格选择更多元。\n" +
                "2. 健康提升：体重减轻可降低患心脏病、糖尿病等疾病的风险，关节压力减小，运动更加轻松。\n" +
                "3. 心理状态：自信心增强，情绪状态可能得到改善，自我满足感和幸福感提升。\n" +
                "4. 生活习惯：饮食结构更健康，规律运动成为生活常态，良好生活习惯得以养成。\n" +
                "\n" +
                "以上是瘦身带来的常见变化，当然具体效果因个体差异而异。";
        String replyContent = "<xml>\n" +
                "  <ToUserName><![CDATA[" + 1 + "]]></ToUserName>\n" +
                "  <FromUserName><![CDATA[" + 2 + "]]></FromUserName>\n" +
                "  <CreateTime>12345678</CreateTime>\n" +
                "  <MsgType><![CDATA[text]]></MsgType>\n" +
                "  <Content><![CDATA[" + xml + "]]></Content>\n" +
                "</xml>";
        Map<String, String> messageMap = MessageUtil.parseXml(replyContent);
        System.out.println(messageMap);
    }

    public String buildReply(String fromUserName,String toUserName,String content){
        String replyContent = "<xml>\n" +
                "  <ToUserName><![CDATA[" + fromUserName + "]]></ToUserName>\n" +
                "  <FromUserName><![CDATA[" + toUserName + "]]></FromUserName>\n" +
                "  <CreateTime>12345678</CreateTime>\n" +
                "  <MsgType><![CDATA[text]]></MsgType>\n" +
                "  <Content><![CDATA[" + content + "]]></Content>\n" +
                "</xml>";
        return replyContent;
    }

}

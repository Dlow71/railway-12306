package com.junjie.index12306.biz.wxservice.handler.message;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Slf4j
public class SubscribeMsgHandler implements WxChatMsgHandler {

    @Override
    public WxChatMsgTypeEnum getMsgType() {
        return WxChatMsgTypeEnum.SUBSCRIBE;
    }

    @Override
    public String dealMsg(Map<String, String> messageMap) {
        log.info("触发用户关注事件！");
        String fromUserName = messageMap.get("FromUserName");
        String toUserName = messageMap.get("ToUserName");
        String subscribeContent = "感谢您的关注，我是信酱！欢迎来到我的编程小屋，我屋里有很多好玩的进来看看吧！\n" +
                "人工智能对话功能测试版已接入，如需使用方式，请回复：ai";
//        String content = "<xml>\n" +
//                "  <ToUserName><![CDATA[" + fromUserName + "]]></ToUserName>\n" +
//                "  <FromUserName><![CDATA[" + toUserName + "]]></FromUserName>\n" +
//                "  <CreateTime>12345678</CreateTime>\n" +
//                "  <MsgType><![CDATA[text]]></MsgType>\n" +
//                "  <Content><![CDATA[" + subscribeContent + "]]></Content>\n" +
//                "</xml>";
        return subscribeContent;
    }

}

package com.junjie.index12306.biz.wxservice.config;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * 本地缓存配置类
 */
@Configuration
public class CacheConfig {
    @Bean("chatApiReplyCache")
    public Cache<String, String> chatApiReplyCache(){
        return Caffeine.newBuilder()
                .expireAfterWrite(120, TimeUnit.SECONDS)
                // Caffeine缓存库在超过最大元素容量（maximumSize）时，并不会抛出异常，
                // 而是遵循LRU（最近最少使用）原则移除最近最少使用的条目
                .maximumSize(50)
                .build();
    }

    @Bean("chatApiAsyncFlagReplyCache")
    public Cache<String, String> chatApiAsyncFlagReplyCache(){
        return Caffeine.newBuilder()
                .expireAfterWrite(120, TimeUnit.SECONDS)
                .maximumSize(50)
                .build();
    }
}

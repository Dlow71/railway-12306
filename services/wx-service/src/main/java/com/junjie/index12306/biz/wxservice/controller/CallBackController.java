package com.junjie.index12306.biz.wxservice.controller;


import com.alibaba.dashscope.exception.InputRequiredException;
import com.alibaba.dashscope.exception.NoApiKeyException;
import com.junjie.index12306.biz.wxservice.handler.chatApi.ChatApiFactory;
import com.junjie.index12306.biz.wxservice.handler.message.WxChatMsgFactory;
import com.junjie.index12306.biz.wxservice.handler.message.WxChatMsgHandler;
import com.junjie.index12306.biz.wxservice.handler.chatApi.QWENChatApi;
import com.junjie.index12306.biz.wxservice.service.CallBackService;
import com.junjie.index12306.biz.wxservice.utils.MessageUtil;
import com.junjie.index12306.biz.wxservice.utils.RedisUtil;
import com.junjie.index12306.biz.wxservice.utils.SHA1;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;


import java.util.Map;
import java.util.Objects;

@RestController
@RequiredArgsConstructor
@Slf4j
public class CallBackController implements InitializingBean {

    // 这个token不管什么样都行
    private String token;
    private final RedisUtil redisUtil;
    private final CallBackService callBackService;


    /**
     * 回调消息校验 只有通过了才会再次发一个post请求，把真正的内容发给我们
     * 接收的参数参考微信开发者文档
     * https://developers.weixin.qq.com/doc/offiaccount/Basic_Information/Access_Overview.html
     */
    @GetMapping("callback")
    public String callback(@RequestParam("signature") String signature,
                           @RequestParam("timestamp") String timestamp,
                           @RequestParam("nonce") String nonce,
                           @RequestParam("echostr") String echostr) {
        log.info("get验签请求参数：signature:{}，timestamp:{}，nonce:{}，echostr:{}",
                signature, timestamp, nonce, echostr);
        // 第四个参数如果消息加解密方式设置的是 安全模式 的话，就要设置一下，这里我们用明文模式可以不用加
        // sha1加密
        String shaStr = SHA1.getSHA1(token, timestamp, nonce, "");
        // 微信传来的签名与我们加密的内容相等，就返回正确参数
        if (signature.equals(shaStr)) {
            return echostr;
        }
        return "unknown";
    }

    /**
     * 接收真正的内容
     * 而且返回的内容是xml格式，所以这里很细节的点是设置produces指定一下数据格式为xml
     */
    @PostMapping(value = "callback", produces = "application/xml;charset=UTF-8")
    public String callback(
            @RequestBody String requestBody,
            @RequestParam(value="signature", required = false) String signature,
            @RequestParam(value="timestamp", required = false) String timestamp,
            @RequestParam(value="nonce", required = false) String nonce,
            @RequestParam(value = "msg_signature", required = false) String msgSignature) {
        String replyContent = callBackService.dealMsg(requestBody);
        return replyContent;
    }


    private final QWENChatApi QWENChatApi;
    @GetMapping("/chat")
    public String chat(String chat) throws NoApiKeyException, InputRequiredException {
        return QWENChatApi.callWithMessage(chat);
    }

    @RequestMapping("/chat/change/{status}")
    public String changeChatApiStatus(@PathVariable("status")String status) {
        redisUtil.set("chat_api_enabled",status.equals("1")? "true":"false");
        return "ok";
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        String wx_gzh_token = redisUtil.get("wx_gzh_token");
        this.token = wx_gzh_token;
    }
}

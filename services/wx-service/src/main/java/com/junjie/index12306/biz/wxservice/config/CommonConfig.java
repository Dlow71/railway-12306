package com.junjie.index12306.biz.wxservice.config;

import com.junjie.index12306.biz.wxservice.handler.chatApi.ChatApiTemplate;
import com.junjie.index12306.framework.starter.bases.ApplicationContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
public class CommonConfig implements CommandLineRunner {
    @Autowired
    private ChatApiTemplate chatApiTemplate;
    @Override
    public void run(String... args) throws Exception {
        chatApiTemplate.setChatApiTemplate(ApplicationContextHolder.getBean(ChatApiTemplate.class));
    }
}

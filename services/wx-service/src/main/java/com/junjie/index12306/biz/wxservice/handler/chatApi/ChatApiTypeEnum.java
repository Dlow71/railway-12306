package com.junjie.index12306.biz.wxservice.handler.chatApi;

public enum ChatApiTypeEnum {

    QWEN("QWEN", "用户关注事件");

    private String chatApiType;

    private String desc;

    ChatApiTypeEnum(String chatApiType, String desc) {
        this.chatApiType = chatApiType;
        this.desc = desc;
    }

    public static ChatApiTypeEnum getApiType(String msgType) {
        for (ChatApiTypeEnum wxChatMsgTypeEnum : ChatApiTypeEnum.values()) {
            if (wxChatMsgTypeEnum.chatApiType.equals(msgType)) {
                return wxChatMsgTypeEnum;
            }
        }
        return null;
    }

}
